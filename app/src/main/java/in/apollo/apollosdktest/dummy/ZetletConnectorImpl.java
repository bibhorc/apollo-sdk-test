package in.apollo.apollosdktest.dummy;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.RoomDatabase;
import in.apollo.android.zetlet.connectors.ZetletConnector;
import in.apollo.apollosdktest.connector.SdkTestConnector;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.android.commons.serverstate.ServerStateManager;

public class ZetletConnectorImpl implements ZetletConnector {

    private final Context context;
    private final DatabaseManager databaseManager;
    private final ServerStateManager serverStateManager;
    private final CommonDeeplinkResolver deeplinkResolver;
    private SdkTestConnector sdkTestConnector;

    public ZetletConnectorImpl(Context context,
                               DatabaseManager databaseManager,
                               ServerStateManager serverStateManager,
                               CommonDeeplinkResolver deeplinkResolver) {
        this.context = context;
        this.databaseManager = databaseManager;
        this.serverStateManager = serverStateManager;
        this.deeplinkResolver = deeplinkResolver;
    }

    public void setSdkTestConnector(SdkTestConnector sdkTestConnector) {
        this.sdkTestConnector = sdkTestConnector;
    }

    @Override
    public Intent handleLinkTap(Context context, String s, boolean b) {
        if (sdkTestConnector == null) {
            return null;
        }
        return sdkTestConnector.handleLinkTap(context, s, b);
    }

    @Nullable
    @Override
    public RoomDatabase getWritableRoomDatabase(String s) {
        return null;
    }

    @Override
    public SQLiteDatabase getWritableUserDatabase() {
        return databaseManager.getWritableUserDatabase();
    }

    @Override
    public Long getServerSyncTime() {
        return serverStateManager.getSyncedEpochTimeInMilliSeconds();
    }

    @Override
    public Integer getDrawableResource(String s) {
        return null;
    }

    @Override
    public List<String> getSortedTemplateCollectionVersionedList() {
        return Arrays.asList("scopeID.1082.templates", "scopeID.1001.templates");
    }

}
