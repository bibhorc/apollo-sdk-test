package in.apollo.apollosdktest.deeplink;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.android.commons.deeplink.DeeplinkResolutionInterface;

public class SdkTestDeeplinkResolver implements DeeplinkResolutionInterface {

    public SdkTestDeeplinkResolver(CommonDeeplinkResolver commonDeeplinkResolver) {
        commonDeeplinkResolver.addDeeplinkResolutionInterface(this);
    }

    @Nullable
    public Intent resolve(Context context, @Nullable Uri uri) {
        if (uri == null) {
            return null;
        }
        return null;
    }

    @Nullable
    @Override
    public Fragment getFragment(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        return null;
    }

}
