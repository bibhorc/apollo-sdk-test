package in.apollo.apollosdktest.dummy;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import in.zeta.android.commons.util.UserDataConnector;

public class DummyUserDataConnnector implements UserDataConnector {

    @NotNull
    @Override
    public String getAuthData() {
        return null;
    }

    @Nullable
    @Override
    public String getSessionToken() {
        return null;
    }

    @NotNull
    @Override
    public byte[] getSharedSecret() {
        return new byte[0];
    }

    @Override
    public boolean hasAuthData() {
        return false;
    }

    @Override
    public boolean isUserLoggedOut() {
        return false;
    }

    @Override
    public void setAuthInfo(@Nullable String s, @Nullable Long aLong, @Nullable String s1) {

    }
}
