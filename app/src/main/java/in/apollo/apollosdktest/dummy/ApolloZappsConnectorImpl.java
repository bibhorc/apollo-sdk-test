package in.apollo.apollosdktest.dummy;

import android.database.sqlite.SQLiteDatabase;

import in.apollo.android.zapps.ApolloZappsConnector;

public class ApolloZappsConnectorImpl implements ApolloZappsConnector {

    private final DatabaseManager databaseManager;

    public ApolloZappsConnectorImpl(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public SQLiteDatabase getWritableUserDatabase() {
        return databaseManager.getWritableUserDatabase();
    }

}
