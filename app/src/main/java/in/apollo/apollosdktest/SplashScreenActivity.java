package in.apollo.apollosdktest;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.apollo.android.screens.deeplink.ScreenCoordinator;
import in.apollo.android.zetlet.TemplateService;
import in.apollo.view_group.ApolloViewGroup;
import in.apollo.view_group.domain.ViewGroup;
import in.apollo.view_group.domain.ViewGroupItem;
import in.apollo.view_group.parameter.ParameterFactory;
import in.zeta.android.commons.DaggerNamedConstants;
import in.zeta.android.commons.concurrency.ThreadPoolService;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.android.commons.util.ExecutorUtils;
import in.zeta.android.commons.util.StringUtils;
import in.zeta.apollo.apollo_user.ApolloUserService;
import in.zeta.apollo.config.ApolloConfigService;
import in.zeta.apollo.themeconfig.ThemeConfigService;
import in.zeta.apollo.userauthmanager.UserAuthManager;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import timber.log.Timber;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.widget.TextView;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import static in.zeta.android.commons.DaggerNamedConstants.BG_POOL_NAME;

public class SplashScreenActivity extends AppCompatActivity {

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private final String STATIC_SCREEN_URL = "test://screen?type=static_screen&viewGroupName=view-group-test-2";
    private final String VERTICAL_SCROLL_URL = "test://screen?type=vertical_scroll&viewGroupName=view-group-test-3";

    private final String DRAWER_SCREEN = "test://drawerScreen?value=%7B%22leftScreen%22%3A+%7B%22url%22%3A%22test%3A%2F%2Fscreen%3Ftype%3Dstatic_screen%26viewGroupName%3Dview-group-test-1%22%7D%2C+%22centerScreen%22%3A+%7B%22url%22%3A%22zeta%3A%2F%2FtabScreen%3Ftype%3Dtab_screen%26viewGroupName%3Dapp_config.appid_1001_bottomBar%22%7D%2C+%22rightScreen%22%3A+%7B%22url%22%3A%22test%3A%2F%2Fscreen%3Ftype%3Dvertical_scroll%26viewGroupName%3Dview-group-test-3%22%7D%7D";

    private final String privateKeyBase64Encoded = "MEECAQAwEwYHKoZIzj0CAQYIKoZIzj0DAQcEJzAlAgEBBCCd5EFfcfciA84maxQRgf+IzbZf252c/ec++29opsW/bQ==";
    private byte[] privateKey;

    @Inject ApolloUserService apolloUserService;
    @Inject ApolloConfigService apolloConfigService;
    @Inject UserAuthManager userAuthManager;
    @Inject ScreenCoordinator screenCoordinator;
    @Inject CommonDeeplinkResolver commonDeeplinkResolver;
    @Inject ApolloViewGroup apolloViewGroup;
    @Inject TemplateService templateService;
    @Inject ThemeConfigService themeConfigService;
    @Inject @Named(BG_POOL_NAME) ThreadPoolService threadPoolService;

    @BindView(R2.id.sub_title) TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ButterKnife.bind(this);

        ApolloSdkTestComponentFactory.getInstance().inject(this);

        startActivity(DummyActivity.getIntent(this));

//        Futures.addCallback(apolloViewGroup.getViewGroups(ParameterFactory.nullViewGroupParameters()), new FutureCallback<List<ViewGroup>>() {
//            @Override
//            public void onSuccess(@NullableDecl List<ViewGroup> viewGroups) {
//                Timber.d("[TEST] view groups: " + gson.toJson(viewGroups));
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                Timber.e(t, "[TEST] failed to get view groups.");
//            }
//        }, MoreExecutors.directExecutor());
//
//        Futures.addCallback(autoLogin(), new FutureCallback<String>() {
//            @Override
//            public void onSuccess(@NullableDecl String result) {
//                ExecutorUtils.uiExecutor().execute(() -> tv.setText("Fetching view groups..."));
//                templateService.syncTemplateCollectionsMandatorily();
//                themeConfigService.syncThemeConfigCollectionOnDemand();
//                threadPoolService.schedule(() -> apolloViewGroup.fetchViewGroups(apolloConfigService.getTenantID(), apolloConfigService.getProjectId(), userAuthManager.getApolloUserSessionId()));
//                Intent intent = commonDeeplinkResolver.resolve(getApplicationContext(), DRAWER_SCREEN);
//                if (intent != null) {
//                    threadPoolService.schedule(() -> startActivity(intent), 3, TimeUnit.SECONDS);
//                } else {
//                    ExecutorUtils.uiExecutor().execute(() -> tv.setText("Failed to resolve deeplink..."));
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                Timber.d("[TEST] auth failed");
//                ExecutorUtils.uiExecutor().execute(() -> tv.setText("Failed to login..."));
//            }
//        }, MoreExecutors.directExecutor());
    }

    private ListenableFuture<String> autoLogin() {
        SettableFuture<String> future = SettableFuture.create();

        String input = "4e0488b9-441b-4901-ad9e-c41b06318e28";
        Timber.d("[TEST] claim: " + input);
        String jwtToken = null;
        try {
            privateKey = Base64.decode(privateKeyBase64Encoded, Base64.DEFAULT);
            jwtToken = createJwtToken(input);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Timber.d("[TEST] jwt = null");
            return Futures.immediateFailedFuture(new RuntimeException("jwt = null"));
        }
        Timber.d("[TEST] jwt: " + jwtToken);

        userAuthManager.setupApolloSdk(jwtToken, false, apolloConfigService.getAuthId());

        Futures.addCallback(apolloUserService.addAppInstallIfNecessary(), new FutureCallback<String>() {
            @Override
            public void onSuccess(@NullableDecl String result) {
                Timber.d("[TEST] Added app install.");
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("[TEST] Failed to add app install.", t);
            }
        }, MoreExecutors.directExecutor());

        Futures.addCallback(userAuthManager.getAuthToken(apolloConfigService.getAuthId()), new FutureCallback<String>() {
            @Override
            public void onSuccess(@NullableDecl String token) {
                if (StringUtils.isNullOrWhiteSpace(token)) {
                    Timber.d("[TEST] Auth Token is null.");
                    future.setException(new RuntimeException("Auth Token is null."));
                    return;
                }
                Timber.d("[TEST] auth token: " + token);
                future.set(token);
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.d("[TEST] get auth token failed");
                future.setException(t);
            }
        }, MoreExecutors.directExecutor());

        return future;
    }

    private String createJwtToken(String claim) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return Jwts.builder()
                .claim("tenantUniqueVectorId", claim)
                .setExpiration(new Date(System.currentTimeMillis() + 5 * 60 * 1000L))
                .signWith(SignatureAlgorithm.ES256, KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(privateKey)))
                .compact();
    }

}