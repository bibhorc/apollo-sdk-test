package in.apollo.apollosdktest.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import in.apollo.apollosdktest.R;
import in.apollo.apollosdktest.R2;
import in.apollo.apollosdktest.util.KeyboardUtil;
import in.apollo.apollosdktest.widgets.BottomSheetListView;
import in.apollo.apollosdktest.widgets.LockableBottomSheetBehavior;
import in.zeta.android.commons.util.DimenUtils;
import in.zeta.android.uicommons.util.ViewUtil;
import in.zeta.android.uicommons.util.animation.AnimationUtil;
import timber.log.Timber;

public class VpaQrFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final int BOTTOM_SHEET_PEEK_HEIGHT_DP = 120;
    private static final long ANIMATION_DURATION_MS = 300;
    private static final float VIEW_ROTATION_ANGLE = 45f;
    private static final TypeEvaluator ARGB_EVALUATOR = new ArgbEvaluator();
    private static final String VPAID_PREFIX = "VPAID:";

    @BindView(R2.id.barcode_scanner) DecoratedBarcodeView barcodeScanner;
    @BindView(R2.id.vpa_id_container) ConstraintLayout vpaIdContainer;
    @BindView(R2.id.vpa_id_et) EditText vpaIdEditText;
    @BindView(R2.id.cross_iv) ImageView crossIcon;
    @BindView(R2.id.vpa_id_iv) ImageView vpaIdIcon;
    @BindView(R2.id.vpa_id_tv) TextView vpaIdTextView;
    @BindView(R2.id.expanded_list_bg) ImageView expandedListBg;
    @BindView(R2.id.arrow_view) ImageView arrowIcon;
    @BindView(R2.id.suggestions_list) BottomSheetListView suggestionsList;
    @BindView(R2.id.suggestions_list_container) LinearLayout suggestionsListContainer;

    private LockableBottomSheetBehavior bottomSheetBehavior;
    private VpaIdAdapter vpaIdAdapter;
    private boolean canSearchNow = false;

    public VpaQrFragment() {
    }

    public static VpaQrFragment newInstance() {
        VpaQrFragment fragment = new VpaQrFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vpa_qr, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initAnimations();
        setupSuggestionsList();
        setupVpaIdEditText();
        setupBarcodeScanner();
        setupBottomSheet();
    }

    @Override
    public void onResume() {
        super.onResume();
        barcodeScanner.resume();
        showDefaultSuggestions();
    }

    @Override
    public void onPause() {
        super.onPause();
        barcodeScanner.pause();
    }

    private void initAnimations() {
        expandedListBg.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.zxing_custom_possible_result_points)));
        crossIcon.setAlpha(0f);
        crossIcon.setRotation(VIEW_ROTATION_ANGLE);
        expandedListBg.setAlpha(0f);
    }

    private void setupVpaIdEditText() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            vpaIdEditText.setVisibility(View.INVISIBLE);
        } else {
            vpaIdEditText.setScaleX(0f);
        }
        vpaIdEditText.setHint("Enter VPA ID");
        vpaIdEditText.setClickable(false);
        vpaIdEditText.setCursorVisible(false);
        vpaIdEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        GradientDrawable vpaIdEtBg = (GradientDrawable) vpaIdEditText.getBackground();
        GradientDrawable vpaIdIconBg = (GradientDrawable) vpaIdIcon.getBackground();
        vpaIdEtBg.setColor(Color.WHITE);
        vpaIdIconBg.setColor(Color.WHITE);
    }

    private void setupBarcodeScanner() {
        try {
            barcodeScanner.decodeContinuous(new BarcodeCallback() {
                @Override
                public void barcodeResult(BarcodeResult result) {
                    if (result.getText().trim().startsWith(VPAID_PREFIX)) {
                        barcodeScanner.pause();
                        ViewUtil.showShortToast(getContext(), result.getText().trim().substring(VPAID_PREFIX.length()));
                    }
                }
            });
        } catch (Exception e) {
            Timber.e("[ASM] VpaQrFragment: failed to scan qr.");
        }
        barcodeScanner.resume();
    }

    private void setupBottomSheet() {
        bottomSheetBehavior = LockableBottomSheetBehavior.from(suggestionsListContainer);
        bottomSheetBehavior.setPeekHeight((int) DimenUtils.dpToPx(getContext(), BOTTOM_SHEET_PEEK_HEIGHT_DP));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            private float alpha = 1f;

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_EXPANDED:
//                        zetaAnalyticsManager.logEvent("Qrcode_Shopid_Bar_Open");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        zetaAnalyticsManager.logEvent("Qrcode_Shopid_Bar_Close");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    alpha = 1f - (slideOffset * 2);
                    ViewUtil.setAlpha(arrowIcon, alpha < 0f ? 0f : alpha);
            }
        });
    }

    private void setupSuggestionsList() {
        suggestionsList.setOnItemClickListener(this);
        vpaIdAdapter = new VpaIdAdapter(getContext(), new ArrayList<>());
        suggestionsList.setAdapter(vpaIdAdapter);
    }

    private void showDefaultSuggestions() {
        List<VpaIdItem> suggestions = Arrays.asList(
                new VpaIdItem("RECENTS", VpaIdItem.Type.HEADER),
                new VpaIdItem(Vpa.builder().setVpaName("McDonalds").setVpaId("mcdonaldsindia@upi").build(), VpaIdItem.Type.SUGGESTION_ITEM),
                new VpaIdItem(Vpa.builder().setVpaName("IDFC").setVpaId("idfc@upi").build(), VpaIdItem.Type.SUGGESTION_ITEM)
        );
        vpaIdAdapter.setNotifyOnChange(false);
        vpaIdAdapter.clear();
        vpaIdAdapter.addAll(suggestions);
        vpaIdAdapter.notifyDataSetChanged();
    }

    @OnTextChanged(R2.id.vpa_id_et)
    void onVpaIdEntered(final CharSequence queryText) {
        if (queryText.toString().trim().length() > 0) {
            List<SuggestionItem> suggestionItems = new CopyOnWriteArrayList<>();
            String vpaId = queryText.toString().trim();
            suggestionItems.add(Vpa.builder().setVpaName("Send to VPA ID").setVpaId(vpaId).build());
            vpaIdAdapter.setNotifyOnChange(false);
            vpaIdAdapter.clear();
            vpaIdAdapter.addAll(suggestionItems);
            vpaIdAdapter.notifyDataSetChanged();
            suggestionsList.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        } else {
            showDefaultSuggestions();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object object = parent.getAdapter().getItem(position);
        if (! (object instanceof VpaIdItem)) {
            return;
        }
        VpaIdItem item = (VpaIdItem) object;
        if (VpaIdItem.Type.SUGGESTION_ITEM.compareTo(item.getType()) != 0) {
            return;
        }
        Vpa vpa = (Vpa) item.getObject();
        ViewUtil.showShortToast(getContext(), vpa.getVpaName());
    }

    @OnClick({R.id.cross_iv, R.id.vpa_id_iv})
    protected void toggleSearchBox(){
        if(canSearchNow){
            setVpaIdEditTextFocus(false);
            vpaIdEditText.setText("");
            vpaIdEditText.setHint("");
            showDefaultSuggestions();
            collapseSuggestionsList();
            animateViews(1f, 0f, Color.WHITE, Color.WHITE, VIEW_ROTATION_ANGLE);
            canSearchNow = false;
        }
        else{
            setVpaIdEditTextFocus(true);
            vpaIdEditText.setHint("Enter VPA ID");
            vpaIdEditText.setText("");
            barcodeScanner.setStatusText("");
            expandSuggestionsList();
            animateViews(0f, 1f, Color.WHITE, Color.WHITE, 0f);
            canSearchNow = true;
        }
    }

    @SuppressLint("NewApi")
    private void animateViews(@FloatRange(from = 0.0, to = 1.0) float from,
                              @FloatRange(from = 0.0, to = 1.0) float to,
                              @ColorInt int fromColor,
                              @ColorInt int toColor,
                              float rotationAngle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int w = vpaIdEditText.getWidth();
            int h = vpaIdEditText.getHeight();
            int radius = (int) Math.hypot(w, h);
            int cy = vpaIdEditText.getHeight()/2;
            if (to == 0f) {
                Animator animator = ViewAnimationUtils.createCircularReveal(vpaIdEditText, w, cy, radius, 0);
                vpaIdEditText.setVisibility(View.VISIBLE);
                animator.setDuration(ANIMATION_DURATION_MS);
                animator.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vpaIdEditText.setVisibility(View.INVISIBLE);
                    }
                });
                animator.start();
            } else {
                Animator animator = ViewAnimationUtils.createCircularReveal(vpaIdEditText, w, cy, 0, radius);
                vpaIdEditText.setVisibility(View.VISIBLE);
                animator.setDuration(ANIMATION_DURATION_MS);
                animator.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
                animator.start();
            }
        } else {
            vpaIdEditText.setPivotX(vpaIdEditText.getX() + vpaIdEditText.getWidth());
            vpaIdEditText.animate()
                    .scaleX(to)
                    .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                    .setDuration(ANIMATION_DURATION_MS)
                    .start();
        }

        ValueAnimator anim = new ValueAnimator();
        anim.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
        anim.setIntValues(fromColor, toColor);
        anim.setEvaluator(ARGB_EVALUATOR);

        anim.setDuration(ANIMATION_DURATION_MS);
        anim.start();

        vpaIdIcon.animate()
                .alpha(from)
                .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                .setDuration(ANIMATION_DURATION_MS)
                .start();
        crossIcon.animate()
                .alpha(to)
                .rotation(rotationAngle)
                .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                .setDuration(ANIMATION_DURATION_MS)
                .start();
        expandedListBg.animate()
                .alpha(to)
                .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                .setDuration(ANIMATION_DURATION_MS)
                .start();
        vpaIdTextView.animate()
                .alpha(from)
                .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                .setDuration(ANIMATION_DURATION_MS)
                .start();
    }

    public void setVpaIdEditTextFocus(boolean isFocused) {
        vpaIdEditText.setCursorVisible(isFocused);
        vpaIdEditText.setClickable(isFocused);
        vpaIdEditText.setFocusable(isFocused);
        vpaIdEditText.setFocusableInTouchMode(isFocused);

        if (isFocused) {
            vpaIdEditText.requestFocus();
            KeyboardUtil.showSoftKeyboard(vpaIdEditText);
        } else {
            KeyboardUtil.hideSoftKeyboard(vpaIdEditText);
        }
    }

    private void collapseSuggestionsList() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) suggestionsList.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        suggestionsList.setLayoutParams(params);

        bottomSheetBehavior.setPeekHeight((int) DimenUtils.dpToPx(getContext(), BOTTOM_SHEET_PEEK_HEIGHT_DP));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.allowDragging(true);
    }

    private void expandSuggestionsList() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) suggestionsList.getLayoutParams();
        params.gravity = Gravity.TOP;
        suggestionsList.setLayoutParams(params);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.allowDragging(false);
    }

}