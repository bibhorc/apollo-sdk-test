package in.apollo.apollosdktest;

import in.apollo.android.screens.ApolloScreensComponentFactory;
import in.apollo.android.zetlet.ApolloZetletComponentFactory;
import in.apollo.apollosdktest.di.ApolloSdkTestComponent;
import in.apollo.apollosdktest.di.DaggerApolloSdkTestComponent;
import in.apollo.view_group.ApolloViewGroupComponentFactory;
import in.zeta.android.commons.ApolloCommonsComponentFactory;
import in.zeta.apollo.apollo_user.ApolloUserComponentFactory;
import in.zeta.apollo.config.ApolloConfigComponentFactory;
import in.zeta.apollo.themeconfig.ApolloThemeConfigComponentFactory;
import in.zeta.apollo.userauthmanager.ApolloUserauthmanagerCompFactory;

public class ApolloSdkTestComponentFactory {

    private static final Object LOCK = new Object();
    private static ApolloSdkTestComponent component = null;

    public static void init() {
        if (component != null) {
            return;
        }

        synchronized (LOCK) {
            if (component == null) {
                component = DaggerApolloSdkTestComponent
                        .factory()
                        .create(ApolloCommonsComponentFactory.getInstance(),
                                ApolloScreensComponentFactory.getInstance(),
                                ApolloViewGroupComponentFactory.getInstance(),
                                ApolloUserauthmanagerCompFactory.getInstance(),
                                ApolloUserComponentFactory.getInstance(),
                                ApolloConfigComponentFactory.getInstance(),
                                ApolloZetletComponentFactory.getInstance(),
                                ApolloThemeConfigComponentFactory.getInstance());
            }
        }
    }

    public static ApolloSdkTestComponent getInstance() {
        if (component == null) {
            throw new RuntimeException("ApolloSdkTestComponent is null.");
        }
        return component;
    }

}
