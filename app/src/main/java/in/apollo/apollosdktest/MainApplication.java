package in.apollo.apollosdktest;

import android.app.Application;
import android.content.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.apollo.android.ApolloZappsComponentFactory;
import in.apollo.android.appconfig.ApolloAppConfigComponentFactory;
import in.apollo.android.location.ApolloLocationComponentFactory;
import in.apollo.android.location.LocationServiceManager;
import in.apollo.android.zetlet.ApolloZetletComponentFactory;
import in.apollo.apollosdktest.dummy.ApolloZappsConnectorImpl;
import in.apollo.apollosdktest.dummy.DatabaseManager;
import in.apollo.apollosdktest.dummy.DummyAnalyticsClient;
import in.apollo.apollosdktest.dummy.DummyUserDataConnnector;
import in.apollo.apollosdktest.dummy.LocationProviderImpl;
import in.apollo.apollosdktest.dummy.TemplateContextProviderImpl;
import in.apollo.apollosdktest.dummy.ThemeConfigConnectorImpl;
import in.apollo.apollosdktest.dummy.ZetaWebViewManagerImpl;
import in.apollo.apollosdktest.dummy.ZetletConnectorImpl;
import in.zeta.android.commons.ApolloCommonsComponentFactory;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.apollo.analytics.ApolloAnalyticsComponentFactory;
import in.zeta.apollo.authmanager.ApolloDoorAuthComponentFactory;
import in.zeta.apollo.collections.ApolloCollectionsComponentFactory;
import in.zeta.apollo.collections.proteus.ApolloCollectionsProteusClientComponentFactory;
import in.zeta.apollo.collections.proteus.di.ApolloCollectionsProteusClientComponent;
import in.zeta.apollo.config.ApolloConfigComponentFactory;
import in.zeta.apollo.gatekeeperdoor.ApolloGatekeeperComponentFactory;
import in.zeta.apollo.gatekeeperdoor.GatekeeperService;
import in.zeta.apollo.gatekeeperproteus.ApolloGatekeeperProteusCompFactory;
import in.zeta.apollo.gatekeeperproteus.GatekeeperProteusClient;
import in.zeta.apollo.permissions.interceptor.ApolloPermissionsInterceptorFactory;
import in.zeta.apollo.permissions.scenario.model.PermissionConfigProvider;
import in.zeta.apollo.permissions.scenario.model.PermissionScenario;
import in.zeta.apollo.themeconfig.ApolloThemeConfigComponentFactory;
import in.zeta.apollo.userauthmanager.ApolloUserauthmanagerCompFactory;
import in.zeta.apollo.widgets.ApolloWidgetsComponentFactory;
import timber.log.Timber;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());

        init();
    }

    private void init() {
        ApolloCommonsComponentFactory.init(this);

        Context context = ApolloCommonsComponentFactory.getInstance().context();
        CommonDeeplinkResolver commonDeeplinkResolver = ApolloCommonsComponentFactory.getInstance().commonDeeplinkResolver();

        ApolloAnalyticsComponentFactory.init(new DummyAnalyticsClient());

        ApolloConfigComponentFactory.getInstance().apolloConfigServiceWrapper().getInstance("pay.json", "configFileName");

        ApolloDoorAuthComponentFactory.init(new DummyUserDataConnnector());

        LocationServiceManager locationServiceManager = ApolloLocationComponentFactory.getInstance().locationServiceManager();
        ApolloUserauthmanagerCompFactory.init(new LocationProviderImpl(locationServiceManager));

        ApolloCollectionsProteusClientComponent apolloCollectionsProteusClientComponent = ApolloCollectionsProteusClientComponentFactory.getInstance();
        ApolloCollectionsComponentFactory.init(apolloCollectionsProteusClientComponent.collectionsClient());

        GatekeeperProteusClient gatekeeperProteusClient = ApolloGatekeeperProteusCompFactory.getInstanceId();
        ApolloGatekeeperComponentFactory.init(
                gatekeeperProteusClient,
                new GatekeeperService.TriggerGKFetchSource() {
                    @Override
                    public void subscribe(GatekeeperService gatekeeperService) {

                    }
                },
                "1001"
        );

        ThemeConfigConnectorImpl themeConfigConnector = new ThemeConfigConnectorImpl(ApolloConfigComponentFactory.getInstance().apolloConfigServiceWrapper().getInstance("pay.json", "configFileName"));
        ApolloThemeConfigComponentFactory.init("theme_config",
                themeConfigConnector,
                new HashMap<>()
        );

        DatabaseManager databaseManager = new DatabaseManager(context);
        ZetletConnectorImpl zetletConnector = new ZetletConnectorImpl(
                context,
                databaseManager,
                ApolloCommonsComponentFactory.getInstance().serverStateManager(),
                commonDeeplinkResolver);

        ApolloZetletComponentFactory.init(
                zetletConnector,
                new TemplateContextProviderImpl(
                        ApolloCommonsComponentFactory.getInstance().context(),
                        ApolloConfigComponentFactory.getInstance().apolloConfigServiceWrapper().getInstance("pay.json", "configFileName")
                ),
                new HashMap<>(),
                false);

        ApolloAppConfigComponentFactory.init("app_config.appid_1001", "scopeID.1001.currency_config");

        ApolloZappsComponentFactory.init(
                new ApolloZappsConnectorImpl(databaseManager),
                "scopeID.1082.shophook_config",
                "zapps.category"
        );

        ZetaWebViewManagerImpl zetaWebViewManager = new ZetaWebViewManagerImpl(
                ApolloCommonsComponentFactory.getInstance().context(),
                ApolloConfigComponentFactory.getInstance().apolloConfigServiceWrapper().getInstance("pay.json", "configFileName")
        );
        ApolloWidgetsComponentFactory.init(zetaWebViewManager);

        ApolloSdkTestComponentFactory.init();
        postInit(themeConfigConnector, zetaWebViewManager, zetletConnector);
    }

    private void postInit(ThemeConfigConnectorImpl themeConfigConnector, ZetaWebViewManagerImpl zetaWebViewManager,
                          ZetletConnectorImpl zetletConnector) {
        zetaWebViewManager.setThemeManager(ApolloThemeConfigComponentFactory.getInstance().themeManager());
        zetletConnector.setSdkTestConnector(ApolloSdkTestComponentFactory.getInstance().sdkTestConnector());
        ApolloLocationComponentFactory.getInstance().locationServiceManager().onAppInForeground();
        ApolloThemeConfigComponentFactory.getInstance().themeConfigService().addThemeConfigListener(themeConfigConnector.getThemeConfigListener());
        ApolloPermissionsInterceptorFactory.setPermissionConfigProvider(new PermissionConfigProvider() {
            @NotNull
            @Override
            public Map<String, PermissionScenario> getPermissionScenarioConfig() {
                return new HashMap<>();
            }

            @Nullable
            @Override
            public ArrayList<String> getPermissionsGroup() {
                return new ArrayList<>();
            }
        });
        ApolloZappsComponentFactory.getInstance().shopHooksService().eagerInit();
        ApolloZetletComponentFactory.getInstance().templateCollectionsListener().init();
    }

}
