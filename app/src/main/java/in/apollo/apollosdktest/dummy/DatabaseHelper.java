package in.apollo.apollosdktest.dummy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import in.apollo.android.zapps.ZappsCategoryTable;
import in.apollo.android.zapps.shophooks.ShopHooksTable;
import in.apollo.android.zetlet.storage.TemplatesTable;
import timber.log.Timber;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String USER_DATABASE_NAME = "zeta.user.db";

    public DatabaseHelper(@Nullable Context context) {
        super(context, USER_DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Timber.d("[PS] DatabaseHelper onCreate called.");
        createAllTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createAllTables(SQLiteDatabase db) {
        ArrayList<String> createQueries = new ArrayList<>();
        createQueries.addAll(ShopHooksTable.getV43CreateQueries());
        createQueries.add(TemplatesTable.getV45CreateQuery());
        createQueries.add(ZappsCategoryTable.getCreateQueryV34());
        executeAllQueries(db, createQueries);
    }

    private void executeAllQueries(SQLiteDatabase db, List<String> queries) {
        for (String query : queries) {
            db.execSQL(query);
        }
    }
}
