package in.apollo.apollosdktest.ui;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

public class Vpa extends SuggestionItem {

    private String vpaName;
    private String vpaId;
    @DrawableRes private Integer avatarResource;
    private Drawable avatarDrawable;
    private String avatarUrl;

    public Vpa(String vpaName, String vpaId, Integer avatarResource,
               Drawable avatarDrawable, String avatarUrl) {
        this.vpaName = vpaName;
        this.vpaId = vpaId;
        this.avatarResource = avatarResource;
        this.avatarDrawable = avatarDrawable;
        this.avatarUrl = avatarUrl;
    }

    public String getVpaName() {
        return vpaName;
    }

    public String getVpaId() {
        return vpaId;
    }

    public Integer getAvatarResource() {
        return avatarResource;
    }

    public Drawable getAvatarDrawable() {
        return avatarDrawable;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String vpaName;
        private String vpaId;
        private Integer avatarResource;
        private Drawable avatarDrawable;
        private String avatarUrl;

        public Builder setVpaName(String vpaName) {
            this.vpaName = vpaName;
            return this;
        }

        public Builder setVpaId(String vpaId) {
            this.vpaId = vpaId;
            return this;
        }

        public Builder setAvatarResource(Integer avatarResource) {
            this.avatarResource = avatarResource;
            return this;
        }

        public Builder setAvatarDrawable(Drawable avatarDrawable) {
            this.avatarDrawable = avatarDrawable;
            return this;
        }

        public Builder setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
            return this;
        }

        public Vpa build() {
            return new Vpa(vpaName, vpaId, avatarResource, avatarDrawable, avatarUrl);
        }
    }

    @Override
    public String getPrimaryText() {
        return vpaName;
    }

    @Nullable
    @Override
    public String getSecondaryText() {
        return vpaId;
    }

    private Vpa(Parcel in) {
        this.vpaName = in.readString();
        this.vpaId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(vpaName);
        dest.writeString(vpaId);
    }

    public static final Parcelable.Creator CREATOR = new Creator<Vpa>() {
        @Override
        public Vpa createFromParcel(Parcel source) {
            return new Vpa(source);
        }

        @Override
        public Vpa[] newArray(int size) {
            return new Vpa[size];
        }
    };

}
