package in.apollo.apollosdktest.di;

import dagger.Component;
import in.apollo.android.screens.di.ApolloScreensComponent;
import in.apollo.android.zetlet.di.ApolloZetletComponent;
import in.apollo.apollosdktest.DummyActivity;
import in.apollo.apollosdktest.SplashScreenActivity;
import in.apollo.apollosdktest.connector.SdkTestConnector;
import in.apollo.view_group.di.ApolloViewGroupComponent;
import in.zeta.android.commons.di.ApolloCommonsComponent;
import in.zeta.apollo.apollo_user.di.ApolloUserComponent;
import in.zeta.apollo.config.di.ApolloConfigComponent;
import in.zeta.apollo.themeconfig.di.ApolloThemeConfigComponent;
import in.zeta.apollo.userauthmanager.di.ApolloUserauthmanagerComponent;

@ApolloSdkTestScope
@Component(
        dependencies = {
                ApolloCommonsComponent.class,
                ApolloScreensComponent.class,
                ApolloViewGroupComponent.class,
                ApolloUserauthmanagerComponent.class,
                ApolloUserComponent.class,
                ApolloConfigComponent.class,
                ApolloZetletComponent.class,
                ApolloThemeConfigComponent.class
        },
        modules = {
                ApolloSdkTestModule.class
        }
)
public interface ApolloSdkTestComponent {

    SdkTestConnector sdkTestConnector();

    void inject(SplashScreenActivity activity);

    void inject(DummyActivity activity);

    @Component.Factory
    interface ComponentFactory {
        ApolloSdkTestComponent create(ApolloCommonsComponent apolloCommonsComponent,
                                      ApolloScreensComponent apolloScreensComponent,
                                      ApolloViewGroupComponent apolloViewGroupComponent,
                                      ApolloUserauthmanagerComponent apolloUserauthmanagerComponent,
                                      ApolloUserComponent apolloUserComponent,
                                      ApolloConfigComponent apolloConfigComponent,
                                      ApolloZetletComponent apolloZetletComponent,
                                      ApolloThemeConfigComponent apolloThemeConfigComponent);
    }

}
