package in.apollo.apollosdktest.dummy;

import android.os.Bundle;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import in.zeta.apollo.analytics.ZetaAnalyticsClient;
import timber.log.Timber;

public class DummyAnalyticsClient implements ZetaAnalyticsClient {

    @Override
    public void logEvent(@NotNull String s) {
        Timber.d(s);
    }

    @Override
    public void logEvent(@NotNull String s, @Nullable Bundle bundle) {
        Timber.d(s);
    }

    @Override
    public void setUserProperty(@NotNull String s, @Nullable String s1) {
        Timber.d(s);
    }

}
