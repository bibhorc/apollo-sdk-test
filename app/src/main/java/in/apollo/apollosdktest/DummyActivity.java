package in.apollo.apollosdktest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import in.apollo.apollosdktest.ui.VpaQrFragment;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.android.uicommons.util.FragmentUtil;
import timber.log.Timber;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

public class DummyActivity extends AppCompatActivity {

    private final String DEMO_TAB_FRAGMENT = "test://screen?type=demo_tab_fragment&viewGroupName=view-group-test-3";

    @Inject CommonDeeplinkResolver commonDeeplinkResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy);

        ApolloSdkTestComponentFactory.getInstance().inject(this);

        loadFragment();
    }

    private void loadFragment() {
//        Fragment fragment = commonDeeplinkResolver.getFragment(this, DEMO_TAB_FRAGMENT);
        Fragment fragment = VpaQrFragment.newInstance();
        if (fragment != null) {
            FragmentUtil.addAndCommitFragmentWithImmediateExecution(getSupportFragmentManager(), R.id.fragment_holder, fragment);
        } else {
            Timber.e("[ASM] tab fragment is null.");
        }
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, DummyActivity.class);
    }

}