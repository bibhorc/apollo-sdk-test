package in.apollo.apollosdktest.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.AppCompatImageView;
import in.zeta.android.uicommons.util.GlideUtil;

public class AvatarImageView extends AppCompatImageView {

    public AvatarImageView(Context context) {
        super(context);
    }

    public AvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AvatarImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setAvatar(@DrawableRes int avatar) {
        setImageResource(avatar);
    }

    public void setAvatar(Drawable avatar) {
        setImageDrawable(avatar);
    }

    public void setAvatar(String avatarUrl, @DrawableRes int defaultAvatar) {
        GlideUtil.load(this, avatarUrl, defaultAvatar);
    }

}
