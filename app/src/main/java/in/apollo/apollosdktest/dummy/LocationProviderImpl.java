package in.apollo.apollosdktest.dummy;

import java.util.HashMap;

import androidx.annotation.NonNull;
import in.apollo.android.location.LocationServiceManager;
import in.zeta.android.commons.util.LocationProvider;

public class LocationProviderImpl implements LocationProvider {

    private final LocationServiceManager locationServiceManager;

    public LocationProviderImpl(LocationServiceManager locationServiceManager) {
        this.locationServiceManager = locationServiceManager;
    }

    @NonNull
    @Override
    public HashMap<String, String> getLocationBundleAsMap() {
        return locationServiceManager.getLocationBundleAsMap();
    }
}
