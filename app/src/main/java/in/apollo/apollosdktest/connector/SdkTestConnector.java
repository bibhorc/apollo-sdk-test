package in.apollo.apollosdktest.connector;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.common.base.Strings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.android.commons.util.StringUtils;
import timber.log.Timber;

public class SdkTestConnector {

    private static final String LAUNCH_IN_BROWSER = "launchInBrowser";
    private static final String SCHEME_XML_PLACEHOLDER = "${ZETA_SCHEME}";
    public static final String KEY_IS_HANDLED = "isHandled";

    private final CommonDeeplinkResolver commonDeeplinkResolver;

    public SdkTestConnector(CommonDeeplinkResolver commonDeeplinkResolver) {
        this.commonDeeplinkResolver = commonDeeplinkResolver;
    }

    @Nullable
    public Intent handleLinkTap(Context context, String uri, boolean isAuthRequired) {
        if (StringUtils.isNullOrWhiteSpace(uri)) {
            return null;
        }
        Uri parsedUri = Uri.parse(uri);
        if (parsedUri != null) {
            String scheme = Strings.nullToEmpty(parsedUri.getScheme());
            if (scheme.equalsIgnoreCase("http") || scheme.equalsIgnoreCase("https")) {
                if (parsedUri.getBooleanQueryParameter(LAUNCH_IN_BROWSER, false)) {
                    Intent intent = getImplicitBrowserIntent(parsedUri);
                    if (intent.resolveActivity(context.getPackageManager()) != null) {
                        return intent;
                    }
                    Timber.d("No browser found.");
                }
            }
        }
        return commonDeeplinkResolver.resolve(context, uri);
    }

    private static Intent getImplicitBrowserIntent(@NonNull Uri uri) {
        return new Intent(Intent.ACTION_VIEW, uri);
    }

}
