package in.apollo.apollosdktest.ui;

public class VpaIdItem {
    private Object object;
    private Type type;

    public VpaIdItem(Object object, Type type) {
        this.object = object;
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        SUGGESTION_ITEM, HEADER
    }
}
