package in.apollo.apollosdktest.dummy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import timber.log.Timber;

public class DatabaseManager {

    private final DatabaseHelper databaseHelper;

    public DatabaseManager(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }

    public SQLiteDatabase getWritableUserDatabase() {
        Timber.d("[PS] DatabaseManager getWritableUserDatabase called.");
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.enableWriteAheadLogging();
        db.execSQL("PRAGMA synchronous=OFF");
        return db;
    }

}
