package in.apollo.apollosdktest.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import in.apollo.apollosdktest.R;

import static in.apollo.apollosdktest.ui.VpaIdItem.Type.SUGGESTION_ITEM;

public class VpaIdAdapter extends ArrayAdapter<VpaIdItem> {

    public VpaIdAdapter(@NonNull Context context, List<VpaIdItem> vpaIdItems) {
        super(context, 0, vpaIdItems);
    }

    @Override
    public int getViewTypeCount() {
        return VpaIdItem.Type.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getType().ordinal();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        VpaIdItem item = getItem(position);
        if (SUGGESTION_ITEM.compareTo(item.getType()) == 0) {
            return getItemView(item, convertView);
        }
        return getHeaderView(item, convertView, parent);
    }

    private View getItemView(VpaIdItem item, @Nullable View convertView) {
        if (convertView == null) {
            convertView = new SuggestionItemView(getContext());
        }
        ((SuggestionItemView) convertView).bindModel((SuggestionItem) item.getObject());
        return convertView;
    }

    private View getHeaderView(VpaIdItem item, @Nullable View convertView,  @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.header_item_v3, parent, false);
        }
        ((TextView) convertView).setText((String) item.getObject());
        return convertView;
    }

    public void addAll(List<SuggestionItem> suggestionItems) {
        List<VpaIdItem> vpaIdItems = new CopyOnWriteArrayList<>();
        for (SuggestionItem suggestionItem : suggestionItems) {
            vpaIdItems.add(new VpaIdItem(suggestionItem, SUGGESTION_ITEM));
        }
        addAll(vpaIdItems);
    }

}
