package in.apollo.apollosdktest.di;

import dagger.Module;
import dagger.Provides;
import in.apollo.apollosdktest.connector.SdkTestConnector;
import in.apollo.apollosdktest.deeplink.SdkTestDeeplinkResolver;
import in.zeta.android.commons.deeplink.CommonDeeplinkResolver;
import in.zeta.apollo.config.ApolloConfigService;
import in.zeta.apollo.config.ApolloConfigServiceWrapper;

@Module
public class ApolloSdkTestModule {

    @Provides
    @ApolloSdkTestScope
    ApolloConfigService provideApolloConfigService(ApolloConfigServiceWrapper apolloConfigServiceWrapper) {
        return apolloConfigServiceWrapper.getInstance("pay.json", "configFileName");
    }

    @Provides
    @ApolloSdkTestScope
    SdkTestConnector sdkTestConnector(CommonDeeplinkResolver commonDeeplinkResolver) {
        return new SdkTestConnector(commonDeeplinkResolver);
    }

    @Provides
    @ApolloSdkTestScope
    SdkTestDeeplinkResolver sdkTestDeeplinkResolver(CommonDeeplinkResolver commonDeeplinkResolver) {
        return new SdkTestDeeplinkResolver(commonDeeplinkResolver);
    }

}
