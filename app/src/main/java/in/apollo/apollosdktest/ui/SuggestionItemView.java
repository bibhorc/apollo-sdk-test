package in.apollo.apollosdktest.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.apollo.apollosdktest.R;
import in.apollo.apollosdktest.R2;
import in.zeta.android.commons.util.StringUtils;

public class SuggestionItemView extends LinearLayout {

    @BindView(R2.id.avatar_view) AvatarImageView avatarView;
    @BindView(R2.id.primary_text) TextView primaryText;
    @BindView(R2.id.secondary_text) TextView secondaryText;

    public SuggestionItemView(Context context) {
        super(context);
        init();
    }

    public SuggestionItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SuggestionItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SuggestionItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.search_item_view, this);
        ButterKnife.bind(this, view);
    }

    public void bindModel(SuggestionItem suggestionItem) {
        primaryText.setText(suggestionItem.getPrimaryText());
        secondaryText.setText(suggestionItem.getSecondaryText());
        if (!StringUtils.isNullOrWhiteSpace(suggestionItem.getAvatarUrl())) {
            avatarView.setAvatar(suggestionItem.getAvatarUrl(), R.drawable.z1_homescreen_user_profilesmall);
        } else if (suggestionItem.getAvatarDrawable() != null) {
            avatarView.setAvatar(suggestionItem.getAvatarDrawable());
        } else if (suggestionItem.getAvatarResource() != null) {
            avatarView.setAvatar(suggestionItem.getAvatarResource());
        }
    }

}
