package in.apollo.apollosdktest.ui;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import in.apollo.android.ui.fragments.BaseFragment;
import in.apollo.apollosdktest.R;
import in.apollo.apollosdktest.R2;
import in.apollo.apollosdktest.widgets.BottomSheetListView;
import in.apollo.apollosdktest.widgets.LockableBottomSheetBehavior;
import in.zeta.android.commons.util.DimenUtils;
import in.zeta.android.uicommons.util.FragmentUtil;
import in.zeta.android.uicommons.util.ProgressDialogUtil;
import in.zeta.android.uicommons.util.ViewUtil;
import in.zeta.android.uicommons.util.animation.AnimationUtil;
import in.zeta.android.uicommons.util.model.Money;
import in.zeta.apollo.widgets.ui.widgets.ZetaWhiteKeyPad;
import timber.log.Timber;

public class SendMoneyQrCodeFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private static final String ADDRESS_NOT_FOUND_EXCEPTION = "AddressNotFoundException";
    private static final String LOCATION_SETTING_ACTION = "android.location.PROVIDERS_CHANGED";
    public static final int INVALID_QR_CODE_AMOUNT = -1;
    private static final String SHOP_ID_PREFIX = "ZSID:";
    public static final String QR_PREFERENCE_KEY = "send_money.prefer_qr";
    private static final String analyticsEventPrefix = "sendMoney.sendToShopId";
    private static final int RECENT_TXN_LIMIT = 5;
    private static final int BOTTOM_SHEET_PEEK_HEIGHT_DP = 80;
    private static final long ANIMATION_DURATION_MS = 300;
    private static final float VIEW_ROTATION_ANGLE = 45f;
    private static final TypeEvaluator ARGB_EVALUATOR = new ArgbEvaluator();
    private static final Handler UI_THREAD_HANDLER = new Handler(Looper.getMainLooper());
    private static final Type MAP_STRING_STRING_TYPE = new TypeToken<HashMap<String, String>>() {}.getType();
    @ColorInt
    private static final int YELLOW_COLOUR = Color.parseColor("#FFBC00");

    private static final String DEEPLINKING_AUTHORITY_QR_CODE_SCREEN = "scanQRCode";

    private static final String PROFILE_KEY_ATTRS = "attrs";
    private static final String PROFILE_KEY_VPAS = "vpas";

    private static final String PROFILE_KEY_VPA = "vpa";
    private static final String PROFILE_KEY_DEVICE_ID = "deviceId";

    private static final String IDFC_UPI_PAYMENT_ENABLED = "app.idfc_upi_enabled";

    @BindView(R2.id.qr_code_scanner)
    DecoratedBarcodeView barcodeView;
    @BindView(R2.id.shop_id_container)
    RelativeLayout shopIdContainer;
    @BindView(R2.id.shop_id_et)
    EditText shopIdEditText;
    @BindView(R2.id.shop_id_iv)
    AppCompatImageView shopIdIcon;
    @BindView(R2.id.shop_id_tv)
    TextView shopIdTv;
    @BindView(R2.id.cross_iv)
    ImageView crossIcon;
    @BindView(R2.id.suggestions_list)
    BottomSheetListView suggestionsList;
    @BindView(R2.id.suggestions_list_container)
    LinearLayout suggestionsListContainer;
    @BindView(R2.id.camera_container)
    ViewGroup cameraContainer;
    @BindView(R2.id.expanded_list_bg)
    ImageView expandedListBg;
    @BindView(R2.id.bottom_sheet_container)
    CoordinatorLayout bottomSheetContainer;
    @BindView(R2.id.zeta_keyboard)
    ZetaWhiteKeyPad keypad;
    @BindView(R2.id.arrow_view)
    ImageView arrowIcon;
    @BindView(R2.id.tv_title_prefix)
    TextView titlePrefix;
    @BindView(R2.id.tv_title_suffix)
    TextView titleSuffix;

    private ProgressDialog progressDialog;
    private Location location;
    private boolean shouldShowBarcodeView = false;
    private boolean isFlashOn = false;
    private boolean canEnterShopId = false;
    private boolean isUPIQrFlowEnabled;
    private LockableBottomSheetBehavior bottomSheetBehavior;
    private GradientDrawable shopIdEtBg;
    private GradientDrawable shopIdIconBg;
    private boolean enableShopIdActions = true;
    private boolean showArrowIcon = false;
    private final Runnable enableShopIdRunnable = () -> {
        enableShopIdActions = true;
    };
    private final Runnable resumeBarcodeView = this::resumeScan;
    private boolean isCameraPermissionDeniedOnce = false;
    private boolean isRequestingCameraPermission = false;
    @Nullable private Money moneyReadFromQrCode;
    @Nullable private String paymentRequestId;

    public SendMoneyQrCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parentView = bindView(inflater, R.layout.fragment_send_money_qr_code, container, false);

        ButterKnife.bind(this, parentView);
        initViewsBasedOnGk();
        try {
            barcodeView.decodeContinuous(new BarcodeCallback() {
                @Override
                public void barcodeResult(BarcodeResult result) {
                }

                @Override
                public void possibleResultPoints(List<ResultPoint> resultPoints) {

                }
            });
        } catch (IllegalStateException exception) {
            Timber.e(exception, "Unable to read QR code");
        }
        initAnimations();
        return parentView;
    }

    public static boolean canHandleUri(Uri uri) {
        return DEEPLINKING_AUTHORITY_QR_CODE_SCREEN.equals(uri.getAuthority());
    }

    private void reportAnalytics(boolean scanSucceeded) {
        zetaAnalyticsManager.logEvent("QRCode_Code_Scanned_" +
            (scanSucceeded ? "Success" : "Failure"), null);
    }

    private void initViewsBasedOnGk() {
        setupSuggestionsList();
        setupSuggestionsBottomSheet();
        setupShopIdEt();
    }

    private void setupShopIdEt() {
        shopIdEditText.setHint("Send Money");
    }

    private void setupSuggestionsList() {
        suggestionsList.setOnItemClickListener(this);
    }

    private void setupSuggestionsBottomSheet() {
        bottomSheetBehavior = LockableBottomSheetBehavior.from(suggestionsListContainer);
        bottomSheetBehavior.setPeekHeight((int) DimenUtils.dpToPx(getContext(), BOTTOM_SHEET_PEEK_HEIGHT_DP));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            private float alpha = 1f;

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_EXPANDED:
                        zetaAnalyticsManager.logEvent("Qrcode_Shopid_Bar_Open");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        zetaAnalyticsManager.logEvent("Qrcode_Shopid_Bar_Close");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull final View bottomSheet, final float slideOffset) {
                if (showArrowIcon) {
                    alpha = 1f - (slideOffset * 2);
                    ViewUtil.setAlpha(arrowIcon, alpha < 0f ? 0f : alpha);
                }
            }
        });
    }

    private void setBarcodeStatusText() {
        titlePrefix.setText("Scan");
        titleSuffix.setText("code");
    }

    @SuppressLint("NewApi")
    private void initAnimations() {
        expandedListBg.setImageDrawable(new ColorDrawable(bindingViewModel.getColorModel().getPrimaryTop()));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            shopIdEditText.setVisibility(View.INVISIBLE);
        } else {
            shopIdEditText.setScaleX(0f);
        }
        shopIdEditText.setClickable(false);
        shopIdEditText.setCursorVisible(false);
        crossIcon.setAlpha(0f);
        crossIcon.setRotation(VIEW_ROTATION_ANGLE);
        expandedListBg.setAlpha(0f);
        shopIdEtBg = (GradientDrawable) shopIdEditText.getBackground();
        shopIdIconBg = (GradientDrawable) shopIdIcon.getBackground();
        shopIdEtBg.setColor(bindingViewModel.getColorModel().getSecondaryColor());
        shopIdIconBg.setColor(bindingViewModel.getColorModel().getSecondaryColor());
    }

    @Override
    protected String getAnalyticsName() {
        return SendMoneyQrCodeFragment.class.getSimpleName();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeScan();
    }

    private void setDefaultSuggestionsOnUI() {
        if (ViewUtil.getString(shopIdEditText).length() == 0) {
            suggestionsList.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onPause() {
        barcodeView.setTorchOff();
        isFlashOn = false;
        barcodeView.pause();
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpProgressDialog();
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.please_wait);
        progressDialog.setMessage("Finding");
    }

    private void dismissProgressDialog() {
        ProgressDialogUtil.dismissSafely(getActivity(), progressDialog);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!FragmentUtil.isFragmentUIActive(this)) {
            //Cache the state to show barcodeView when the UI becomes active
            //This is needed because onStart()/onResume() is called after setUserVisibleHint(boolean)
            //Camera won't turn on if this case is not handled
            shouldShowBarcodeView = isVisibleToUser;
            return;
        }
        if (isVisibleToUser) {
            resumeScan();
            return;
        }
        barcodeView.pause();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    @OnTextChanged(R2.id.shop_id_et)
    void getSuggestions(final CharSequence queryText) {
    }

    @OnClick({R2.id.shop_id_iv, R2.id.cross_iv})
    protected void toggleShopIdField() {
        if (!enableShopIdActions) {
            return;
        }
        if (canEnterShopId) {
            shopIdEditText.setText("");
            setDefaultSuggestionsOnUI();
            collapseSuggestionsList();
            setShopIdEditTextFocus(false);
            shopIdEditText.setHint("");
            setBarcodeStatusText();
            animateViews(1f, 0f, Color.WHITE, bindingViewModel.getColorModel().getSecondaryColor(), VIEW_ROTATION_ANGLE);
            canEnterShopId = false;
        } else {
            expandSuggestionsList();
            setShopIdEditTextFocus(true);
            shopIdEditText.setHint("Send Money");
            barcodeView.setStatusText("");
            animateViews(0f, 1f, bindingViewModel.getColorModel().getSecondaryColor(), Color.WHITE, 0f);
            canEnterShopId = true;
        }
        enableShopIdActions = false;
        UI_THREAD_HANDLER.postDelayed(enableShopIdRunnable, ANIMATION_DURATION_MS);
        zetaAnalyticsManager.logEvent("Qrcode_Shopid_Icon_Tapped");
    }

    /**
     * Animate between shop ID and QR code views.
     * From and to will decide the alpha of bg, scaling of edit text, width of the shop ID edit text and the visibility of cross icon/shop Id icon.
     * For API >= 21, we use circular reveal for showing/hiding shop ID edit text. The same will be achieved in API < 21 by scale animation.
     * Reason: Circular reveal looks better and more natural than scale animation.
     *
     * @param from          Decides the alpha of shop ID icon
     * @param to            Decides the scale of shop ID edit text and the alpha of cross icon and recent transactions list view.
     * @param fromColor     Color from which shop ID edit text should animate.
     * @param toColor       Color to which shop ID edit text should animate.
     * @param rotationAngle Angle to which the cross button should be rotated when expanding/collapsing the shop ID edit text.
     */
    @SuppressLint("NewApi")
    private void animateViews(@FloatRange(from = 0.0, to = 1.0) float from,
                              @FloatRange(from = 0.0, to = 1.0) float to,
                              @ColorInt int fromColor,
                              @ColorInt int toColor,
                              float rotationAngle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int w = shopIdEditText.getWidth();
            int h = shopIdEditText.getHeight();
            int radius = (int) Math.hypot(w, h);
            int cy = shopIdEditText.getHeight() / 2;
            if (to == 0f) {
                Animator animator = ViewAnimationUtils.createCircularReveal(shopIdEditText, w, cy, radius, 0);
                shopIdEditText.setVisibility(View.VISIBLE);
                animator.setDuration(ANIMATION_DURATION_MS);
                animator.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        shopIdEditText.setVisibility(View.INVISIBLE);
                    }
                });
                animator.start();
            } else {
                Animator animator = ViewAnimationUtils.createCircularReveal(shopIdEditText, w, cy, 0, radius);
                shopIdEditText.setVisibility(View.VISIBLE);
                animator.setDuration(ANIMATION_DURATION_MS);
                animator.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
                animator.start();
            }
        } else {
            shopIdEditText.setPivotX(shopIdEditText.getX() + shopIdEditText.getWidth());
            shopIdEditText.animate()
                .scaleX(to)
                .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
                .setDuration(ANIMATION_DURATION_MS)
                .start();
        }

        ValueAnimator anim = new ValueAnimator();
        anim.setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR);
        anim.setIntValues(fromColor, toColor);
        anim.setEvaluator(ARGB_EVALUATOR);
        anim.addUpdateListener(valueAnimator -> {
            shopIdEtBg.setColor((Integer) valueAnimator.getAnimatedValue());
            shopIdIconBg.setColor((Integer) valueAnimator.getAnimatedValue());
        });

        anim.setDuration(ANIMATION_DURATION_MS);
        anim.start();

        shopIdIcon.animate()
            .alpha(from)
            .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
            .setDuration(ANIMATION_DURATION_MS)
            .start();
        crossIcon.animate()
            .alpha(to)
            .rotation(rotationAngle)
            .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
            .setDuration(ANIMATION_DURATION_MS)
            .start();
        expandedListBg.animate()
            .alpha(to)
            .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
            .setDuration(ANIMATION_DURATION_MS)
            .start();
        shopIdTv.animate()
            .alpha(from)
            .setInterpolator(AnimationUtil.EASE_IN_OUT_INTERPOLATOR)
            .setDuration(ANIMATION_DURATION_MS)
            .start();

    }

    private void expandSuggestionsList() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) suggestionsList.getLayoutParams();
        params.gravity = Gravity.TOP;
        suggestionsList.setLayoutParams(params);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.allowDragging(false);
    }

    private void collapseSuggestionsList() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) suggestionsList.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        suggestionsList.setLayoutParams(params);

        bottomSheetBehavior.setPeekHeight((int) ViewUtil.dpToPx(getContext(), BOTTOM_SHEET_PEEK_HEIGHT_DP));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.allowDragging(true);
    }

    public void setShopIdEditTextFocus(boolean isFocused) {
        shopIdEditText.setCursorVisible(isFocused);
        shopIdEditText.setClickable(isFocused);
        shopIdEditText.setFocusable(isFocused);
        shopIdEditText.setFocusableInTouchMode(isFocused);

        if (isFocused) {
            shopIdEditText.requestFocus();
            showKeyboard();
        } else {
            hideKeyboard();
        }
    }

    public void hideKeyboard() {
        keypad.setVisibility(View.GONE);

    }

    private void showKeyboard() {
        keypad.setVisibility(View.VISIBLE);
    }

    @SuppressLint("CheckResult")
    private void resumeScan() {
        barcodeView.resume();
    }

    @NonNull
    public static Fragment getFragmentDetails(Uri uri) {
        return new SendMoneyQrCodeFragment();
    }
}
