package in.apollo.apollosdktest.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import timber.log.Timber;

/**
 *Bottom sheets can be swiped down/up by the user to expand/collapse it correspondingly.
 * Lockable bottom sheet allows swipe based on allowDragging(boolean) method.
 * If dragging is disabled, user's swipe has no effect on the bottom sheet.
 * If dragging is enabled, it behaves as a normal bottom sheet.
 * Usage: When the bottom sheet layout needs to be locked in some position, not allowing user drag to take effect
 */
public class LockableBottomSheetBehavior<V extends View> extends BottomSheetBehavior<V> {

    private boolean allowDrag = true;

    public LockableBottomSheetBehavior() {}

    public LockableBottomSheetBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void allowDragging(boolean allowDrag) {
        this.allowDrag = allowDrag;
    }

    public boolean isDraggingAllowed() {
        return allowDrag;
    }

    @Override
    public boolean onInterceptTouchEvent(CoordinatorLayout parent, V child, MotionEvent event) {
        boolean handled = false;
        if (allowDrag) {
            try {
                handled = super.onInterceptTouchEvent(parent, child, event);
            } catch (NullPointerException e) {
                Timber.e(e, "Unable to intercept touch event");
            }
        }
        return handled;
    }

    @Override
    public boolean onTouchEvent(CoordinatorLayout parent, V child, MotionEvent event) {
        boolean handled = false;
        if (allowDrag) {
            handled = super.onTouchEvent(parent, child, event);
        }
        return handled;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, V child, View directTargetChild, View target, int nestedScrollAxes) {
        boolean handled = false;
        if (allowDrag) {
            handled = super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
        }
        return handled;
    }

    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, V child, View target, int dx, int dy, int[] consumed) {
        if (allowDrag) {
            super.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed);
        }
    }

    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, V child, View target) {
        if (allowDrag) {
            super.onStopNestedScroll(coordinatorLayout, child, target);
        }
    }

    @Override
    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, V child, View target, float velocityX, float velocityY) {
        boolean handled = false;
        if (allowDrag) {
            handled = super.onNestedPreFling(coordinatorLayout, child, target, velocityX, velocityY);
        }
        return handled;

    }

    public static <V extends View> LockableBottomSheetBehavior<V> from(V view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (!(params instanceof CoordinatorLayout.LayoutParams)) {
            throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
        }
        CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) params)
            .getBehavior();
        if (!(behavior instanceof LockableBottomSheetBehavior)) {
            throw new IllegalArgumentException(
                "The view is not associated with LockableBottomSheetBehavior");
        }
        return (LockableBottomSheetBehavior<V>) behavior;
    }
}
