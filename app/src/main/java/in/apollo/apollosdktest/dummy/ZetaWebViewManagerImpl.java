package in.apollo.apollosdktest.dummy;

import android.content.Context;
import android.net.Uri;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import in.apollo.android.localisation.LocaleManager;
import in.zeta.apollo.config.ApolloConfigService;
import in.zeta.apollo.themeconfig.ThemeManager;
import in.zeta.apollo.widgets.ZetaWebviewManager;

public class ZetaWebViewManagerImpl implements ZetaWebviewManager {

    private static final String APP_ID = "appID";
    private static final String COUNTRY_CODE = "countryCode";
    private static final String LOCALE = "locale";
    private static final String THEME_ID = "themeID";
    private static final String CURRENCY = "currency";
    private static final String SUPPORTED_LOCALES = "supportedLocales";
    private static final String ESP_ID = "espID";

    private final Context context;
    private final ApolloConfigService apolloConfigService;

    private ThemeManager themeManager;

    public ZetaWebViewManagerImpl(Context context,
                                  ApolloConfigService apolloConfigService) {
        this.context = context;
        this.apolloConfigService = apolloConfigService;
    }

    @NonNull
    private Map<String, String> getCommonQueryParamMap() {
        Map<String, String> commonQueryParamMap = new HashMap<>();

        JsonObject config = apolloConfigService.getConfig();

        commonQueryParamMap.put(APP_ID, String.valueOf(apolloConfigService.getProjectId()));
        commonQueryParamMap.put(COUNTRY_CODE, config.get(COUNTRY_CODE).getAsString());
        commonQueryParamMap.put(LOCALE, LocaleManager.getLocale(context));
        commonQueryParamMap.put(THEME_ID, themeManager.getThemeID());
        commonQueryParamMap.put(CURRENCY, config.get(CURRENCY).getAsString());
        return commonQueryParamMap;
    }

    @Override
    public Uri addCommonQueryParamsToUri(@NonNull Uri uri) {
        return uri;
    }

    public void setThemeManager(ThemeManager themeManager) {
        this.themeManager = themeManager;
    }

}
