package in.apollo.apollosdktest.dummy;

import com.google.gson.JsonObject;

import in.zeta.apollo.config.ApolloConfigService;
import in.zeta.apollo.themeconfig.ThemeConfigService;
import in.zeta.apollo.themeconfig.connector.ThemeConfigConnector;
import io.reactivex.subjects.PublishSubject;

public class ThemeConfigConnectorImpl implements ThemeConfigConnector {

    private PublishSubject<Boolean> themeUpdatesObservable = PublishSubject.create();
    private ThemeConfigService.ThemeConfigListener themeConfigListener = () -> themeUpdatesObservable.onNext(true);

    private final ApolloConfigService apolloConfigService;

    public ThemeConfigConnectorImpl(ApolloConfigService apolloConfigService) {
        this.apolloConfigService = apolloConfigService;
    }

    @Override
    public String getThemeID() {
        JsonObject sdkConfig = apolloConfigService.getConfig();
        return sdkConfig != null && sdkConfig.has("themeId") ? sdkConfig.get("themeId").getAsString() : "1";
    }

    @Override
    public String getThemeID(Long aLong) {
        return getThemeID();
    }

    @Override
    public PublishSubject<Boolean> getThemeUpdatesObservable() {
        return themeUpdatesObservable;
    }

    public ThemeConfigService.ThemeConfigListener getThemeConfigListener() {
        return themeConfigListener;
    }

}
