package in.apollo.apollosdktest.dummy;

import android.content.Context;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.StringDef;
import in.apollo.android.localisation.LocaleManager;
import in.apollo.android.zetlet.interfaces.TemplateContextProvider;
import in.zeta.apollo.config.ApolloConfigService;

public class TemplateContextProviderImpl implements TemplateContextProvider {

    private final Context context;
    private final ApolloConfigService apolloConfigService;

    public TemplateContextProviderImpl(Context context,
                                       ApolloConfigService apolloConfigService) {
        this.context = context;
        this.apolloConfigService = apolloConfigService;
    }

    @NotNull
    @Override
    public String getCurrentLanguage() {
        return LocaleManager.getLanguage(context).toUpperCase();
    }

    @NotNull
    @Override
    public String getCurrentViewType() {
        return "UIVIEW";
    }

    @NotNull
    @Override
    public JsonObject getDataForResolution() {
        JsonObject data = new JsonObject();
        data.addProperty(ContextKeys.KEY_APP_ID, 1001L);
        return data;
    }

    @StringDef({ContextKeys.KEY_APP_ID})
    public @interface ContextKeys {
        String KEY_APP_ID = "appid";
    }
}
