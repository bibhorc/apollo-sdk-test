package in.apollo.apollosdktest.ui;

import android.graphics.drawable.Drawable;
import android.os.Parcelable;

import java.util.Comparator;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

public abstract class SuggestionItem implements Parcelable {

    private static Comparator<SuggestionItem> comparator = new Comparator<SuggestionItem>() {
        @Override
        public int compare(SuggestionItem lhs, SuggestionItem rhs) {
            int first = lhs.getPrimaryText().compareToIgnoreCase(rhs.getPrimaryText());
            String secondaryText = lhs.getSecondaryText() == null ? "" : lhs.getSecondaryText();
            String otherSecondaryText = rhs.getSecondaryText() == null ? "" : rhs.getSecondaryText();
            return first == 0 ? secondaryText.compareToIgnoreCase(otherSecondaryText) : first;
        }
    };

    public static Comparator<SuggestionItem> getComparator() {
        return comparator;
    }

    public abstract String getPrimaryText();

    @Nullable
    public abstract String getSecondaryText();

    @DrawableRes
    public abstract Integer getAvatarResource();

    public abstract Drawable getAvatarDrawable();

    public abstract String getAvatarUrl();

}
